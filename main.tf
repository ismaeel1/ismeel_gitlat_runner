data "template_cloudinit_config" "config2" {
  gzip          = false
  base64_encode = false  #first part of local config2 file
  part {
    content_type = "text/x-shellscript"
    content      = <<-EOF
    #!/bin/bash
    sudo su 
    sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
    sudo chmod +x /usr/local/bin/gitlab-runner
    sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
    sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
    sudo gitlab-runner start
    gitlab-runner register --non-interactive --url "https://gitlab.com/" --registration-token "Muy1R5FyUfUKy_Qy7dDm" --executor "shell"  --tag-list "raza1,raza2" --description "ismaeel_gitlab-runner"
    curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
    sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
    sudo apt install terraform
    sudo rm -rf /home/gitlab-runner/.bash_logout
    EOF
  }

}


resource "aws_autoscaling_group" "autoscalling_group_config2" {
  name = "ismaeel_autoscaling_runner"
  max_size = 3
  min_size = 2
#  health_check_grace_period = 300
#  health_check_type = "EC2"
  desired_capacity = 3
  force_delete = true
  vpc_zone_identifier = ["subnet-eaa81381","subnet-09c93874"]
  launch_configuration = aws_launch_configuration.runner_launch_configuration.name
 
  lifecycle {
    create_before_destroy = true
  }
}



resource"aws_launch_configuration" "runner_launch_configuration" {
  name = "ismaeel_launchConfiguration_runner"
  image_id = "ami-00dfe2c7ce89a450b"
  instance_type = "t2.micro"
  key_name = "ismaeeluseast2kp"
  iam_instance_profile= aws_iam_instance_profile.test_profile.name
  security_groups = [aws_security_group.runner_SG.id]
  user_data = data.template_cloudinit_config.config2.rendered
                  
}


resource "aws_security_group" "runner_SG" {
  vpc_id      = "vpc-57fb683c "
  name        = "ismaeeel_runner_SG"
  description = "security group that allows ssh and all egress traffic"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }



  tags = {
    Name = "ismaeel_runner_SG"
  }
}






resource "aws_iam_role" "test_role" {
  name = "gitlab-shared-runner-iam-role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

inline_policy {
  name   = "SharedRunnerPipelineAccess"
  policy = data.aws_iam_policy_document.inline_policy.json
}

}




data "aws_iam_policy_document" "inline_policy" {
  statement {
    actions   = ["ec2:*","autoscaling:*","cloudformation:*","apigateway:*","elasticloadbalancing:*"]
    resources = ["*"]
  }
}


data "aws_iam_policy" "ReadOnlyAccess" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

resource "aws_iam_role_policy_attachment" "sto-readonly-role-policy-attach" {
  role       = "${aws_iam_role.test_role.name}"
  policy_arn = "${data.aws_iam_policy.ReadOnlyAccess.arn}"
}


resource "aws_iam_instance_profile" "test_profile" {
  name = "test_profile"
  role = aws_iam_role.test_role.name
}
